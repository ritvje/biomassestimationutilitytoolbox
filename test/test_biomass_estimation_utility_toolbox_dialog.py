# coding=utf-8
"""Dialog test.

.. note:: This program is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation; either version 2 of the License, or
     (at your option) any later version.

"""
import unittest
from PyQt4.QtGui import QDialogButtonBox, QDialog
from biomass_estimation_utility_toolbox_dialog import BiomassEstimationUtilityToolboxDialog
from utilities import get_qgis_app
QGIS_APP = get_qgis_app()

__author__ = 'jenna.ritvanen@gmail.com'
__date__ = '2016-05-23'
__copyright__ = 'Copyright 2016, Jenna Ritvanen'


class BiomassEstimationUtilityToolboxDialogTest(unittest.TestCase):
    """Test dialog works."""

    def setUp(self):
        """Runs before each test."""
        self.dialog = BiomassEstimationUtilityToolboxDialog(None)

    def tearDown(self):
        """Runs after each test."""
        self.dialog = None

    def test_dialog_close(self):
        """Test we can click close."""
        button = self.dialog.button_box.button(QDialogButtonBox.Close)
        button.click()
        result = self.dialog.result()
        self.assertEqual(result, QDialog.Rejected)

if __name__ == "__main__":
    suite = unittest.makeSuite(BiomassEstimationUtilityToolboxDialogTest)
    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(suite)
