# coding=utf-8
"""Test data creation.

.. note:: This program is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation; either version 2 of the License, or
     (at your option) any later version.

"""
import numpy as np
import unittest
from create_test_data import test_data
import feature_algorithms

__author__ = 'jenna.ritvanen@gmail.com'
__date__ = '2016-05-23'
__copyright__ = 'Copyright 2016, Jenna Ritvanen'


class FeatureCalculationTest(unittest.TestCase):
    """Test feature calculations."""

    def setUp(self):
        """Runs before each test."""
        self.test_data = test_data()

    def tearDown(self):
        """Runs after each test."""
        self.test_data = None

    def test_angular_second_moment(self):
        """Test angular second motion calculation"""
        value = feature_algorithms.asm(self.test_data.data, 'band_1')
        # Test with 6 decimal accuracy, bc comparison value calculated by hand/Matlab & has different number of decimals
        self.assertEqual(np.round(value, 6), np.round(0.050045913682277, 6))

    def test_contrast(self):
        """Test contrast calculation"""
        value = feature_algorithms.contrast(self.test_data.data, 'band_1')
        # Test with 6 decimal accuracy, bc comparison value calculated by hand/Matlab & has different number of decimals
        self.assertEqual(np.round(value, 6), np.round(8.272727272727273, 6))

    def test_correlation(self):
        """Test correlation calculation"""
        value = feature_algorithms.correlation(self.test_data.data, 'band_1')
        # Test with 6 decimal accuracy, bc comparison value calculated by hand/Matlab & has different number of decimals
        self.assertEqual(np.round(value, 6), np.round(8.387799750000029e+06, 6))

    def test_sum_of_squares(self):
        """Test sum of squares (variance) calculation"""
        value = feature_algorithms.ssq(self.test_data.data, 'band_1')
        # Test with 6 decimal accuracy, bc comparison value calculated by hand/Matlab & has different number of decimals
        self.assertEqual(np.round(value, 6), np.round(38.268887878787886, 6))

    def test_inverse_difference_moment(self):
        """Test inverse difference moment calculation"""
        value = feature_algorithms.inv_diff_moment(self.test_data.data, 'band_1')
        # Test with 6 decimal accuracy, bc comparison value calculated by hand/Matlab & has different number of decimals
        self.assertEqual(np.round(value, 6), np.round(0.455654101995565, 6))

    def test_sum_average(self):
        """Test sum average calculation"""
        value = feature_algorithms.sum_avg(self.test_data.data, 'band_1')
        # Test with 6 decimal accuracy, bc comparison value calculated by hand/Matlab & has different number of decimals
        self.assertEqual(np.round(value, 6), np.round(10.999999999999998, 6))

    def test_sum_variance(self):
        """Test sum variance calculation"""
        value = feature_algorithms.sum_var(self.test_data.data, 'band_1')
        # Test with 6 decimal accuracy, bc comparison value calculated by hand/Matlab & has different number of decimals
        self.assertEqual(np.round(value, 6), np.round(86.183865865548938, 6))

    def test_sum_entropy(self):
        """Test sum entropy calculation"""
        value = feature_algorithms.sum_entropy(self.test_data.data, 'band_1')
        # Test with 6 decimal accuracy, bc comparison value calculated by hand/Matlab & has different number of decimals
        self.assertEqual(np.round(value, 6), np.round(3.129711465065293, 6))

    def test_entropy(self):
        """Test entropy calculation"""
        value = feature_algorithms.entropy(self.test_data.data, 'band_1')
        # Test with 6 decimal accuracy, bc comparison value calculated by hand/Matlab & has different number of decimals
        self.assertEqual(np.round(value, 6), np.round(4.321246988232698, 6))

    def test_difference_variance(self):
        """Test difference variance calculation"""
        value = feature_algorithms.diff_var(self.test_data.data, 'band_1')
        # Test with 6 decimal accuracy, bc comparison value calculated by hand/Matlab & has different number of decimals
        self.assertEqual(np.round(value, 6), np.round(6.947622555784225, 6))

    def test_difference_entropy(self):
        """Test difference entropy calculation"""
        value = feature_algorithms.diff_entropy(self.test_data.data, 'band_1')
        # Test with 6 decimal accuracy, bc comparison value calculated by hand/Matlab & has different number of decimals
        self.assertEqual(np.round(value, 6), np.round(0.439496986921513, 6))

    def test_information_measure_of_correlation_1(self):
        """Test first information measure of correlation calculation"""
        value = feature_algorithms.info_meas_corr_1(self.test_data.data, 'band_1')
        # Test with 6 decimal accuracy, bc comparison value calculated by hand/Matlab & has different number of decimals
        self.assertEqual(np.round(value, 6), np.round(-0.699058554524621, 6))

    def test_information_measure_of_correlation_2(self):
        """Test second information measure of correlation calculation"""
        value = feature_algorithms.info_meas_corr_2(self.test_data.data, 'band_1')
        # Test with 6 decimal accuracy, bc comparison value calculated by hand/Matlab & has different number of decimals
        self.assertEqual(np.round(value, 6), np.round(0.995178944513496, 6))

    def test_max_corr_coeff(self):
        """Test maximal correlation coefficient calculation"""
        value = feature_algorithms.max_corr_coeff(self.test_data.data, 'band_1')
        # Test with 6 decimal accuracy, bc comparison value calculated by hand/Matlab & has different number of decimals
        self.assertEqual(np.round(value, 6), np.round(1.000000000000000, 6))

if __name__ == '__main__':
    unittest.main()
