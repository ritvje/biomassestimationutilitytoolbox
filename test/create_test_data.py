# coding=utf-8
"""Test data creation.

.. note:: This program is free software; you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation; either version 2 of the License, or
     (at your option) any later version.

"""
import numpy as np
from skimage.feature import greycomatrix

__author__ = 'jenna.ritvanen@gmail.com'
__date__ = '2016-05-23'
__copyright__ = 'Copyright 2016, Jenna Ritvanen'


class test_data:
    def __init__(self, x_size=100, y_size=50, start=0, end=9):
        """
        Initialize test data
        """
        self.data = {}

        self.data['bands'] = {}
        self.data['bands']['band_1'] = {}

        self.__create_steady_data(x_size, y_size, start, end)

    def __create_steady_data(self, x_size, y_size, start, end):
        """
        Steady data returns always the same result with same parameters. The values from start to end (both including) are repeated in order to array of size x_size x y_size. Also calculates GLCM for created data
        """
        size = x_size * y_size
        number_of_values = end - start + 1
        array = np.zeros(size, dtype=np.int)

        val = start
        for i in range(size):
            array[i] = val
            val += 1
            if val > end:
                val = start
            pass

        array = array.reshape([y_size, x_size])
        self.data['bands']['band_1']['pixels'] = array

        self.data['glcm_levels'] = number_of_values - 1
        self.data['glcm_angle'] = 0
        self.data['glcm_distance'] = 1
        self.data['glcm_symmetric'] = True

        max_val = np.nanmax(array)
        array = np.nan_to_num(array)
        result = greycomatrix(array, [1], [0], number_of_values, True, True)  # the glcm is always normalized
        self.data['bands']['band_1']['glcm'] = result[:, :, 0, 0]
