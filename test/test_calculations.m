%%% test_calculations.m
% Author: Jenna Ritvanen, 2016
% This file includes all calculations for all values used in
% test_feature_calculations.py
% The data used in the tests is stored in test_data.mat


%% angular second moment
a = bands.band_1.glcm;
Ng = size(a,1);

asm = sum(sum(a.^2))


%% contrast
a = bands.band_1.glcm;
Ng = size(a,1);
totsum = 0;

for n = 0:Ng-1
   summa = 0;
   for i = 1:Ng
       for j = 1:Ng
           if abs(i-j) == n
               summa = summa + a(i,j);
           end
       end
   end
   totsum = totsum + n^2*summa;
end

contrast = totsum

%% correlation
a = bands.band_1.glcm;
Ng = size(a,1);

myyx = mean(sum(a));
myyy = mean(sum(a,2));

stdx = std(sum(a), 1);
stdy = std(sum(a,2), 1);

summa = 0;
for i = 1:Ng
    for j = 1:Ng
        summa = summa + i*j*a(i,j);
    end
end

corr = (summa-myyx*myyy)/(stdx*stdy)


%% sum of squares
a = bands.band_1.glcm;
Ng = size(a,1);

myy = mean(reshape(a, size(a,1)*size(a,2),1));

summa = 0;
for i = 1:Ng
    for j = 1:Ng
        summa = summa + (i-myy)^2*a(i,j);
    end
end

ssq = summa

%% inverse difference moment
a = bands.band_1.glcm;
Ng = size(a,1);

summa = 0;
for i = 1:Ng
    for j = 1:Ng
        summa = summa + 1/(1+(i-j)^2) * a(i,j);
    end
end

idm = summa

%% sum average
a = bands.band_1.glcm;
Ng = size(a,1);

summa = 0;
for k = 2:(2*Ng)
    tsum = 0;
    for i = 1:Ng
        for j = 1:Ng
            if i+j == k
                tsum = tsum + a(i,j);
            end
        end
    end
    summa = summa + k*tsum;
end

savg = summa

%% sum entropy
a = bands.band_1.glcm;
Ng = size(a,1);

summa = 0;
for k = 2:(2*Ng)
  tsum = 0;
  for i = 1:Ng
    for j = 1:Ng
      if i+j == k
        tsum = tsum + a(i,j);
      end
    end
  end
  summa = summa + tsum*log2(tsum + eps(1));
end
sum_ent = -1*summa

%% sum variance (calculate sum entropy first and use it here)
a = bands.band_1.glcm;
Ng = size(a,1);

summa = 0;
for k = 2:(2*Ng)
    tsum = 0;
    for i = 1:Ng
        for j = 1:Ng
            if i+j == k
                tsum = tsum + a(i,j);
            end
        end
    end
    summa = summa + (k - sum_ent)^2*tsum;
end

svar = summa



%% entropy
a = bands.band_1.glcm;
Ng = size(a,1);

summa = 0;
for i = 1:Ng
    for j = 1:Ng
        summa = summa + a(i,j)*log2(a(i,j)+eps(1));
    end
end
ent = -1* summa

%% difference entropy
a = bands.band_1.glcm;
Ng = size(a,1);

summa = 0;
for k = 0:Ng-1
    tsum = 0;
    for i = 1:Ng
        for j = 1:Ng
            if abs(i-j) == k
                tsum = tsum + a(i,j);
            end
        end
    end
    summa = summa + tsum*log2(tsum+eps(1));
end
diff_ent = -1*summa

%% difference variance (use entropy from above)
a = bands.band_1.glcm;
Ng = size(a,1);


summa = 0;
for k = 0:Ng-1
    tsum = 0;
    for i = 1:Ng
        for j = 1:Ng
            if abs(i-j) == k
                tsum = tsum + a(i,j);
            end
        end
    end
    summa = summa + (k - diff_ent)^2*tsum;
end

difvar = summa

%% information measures of correlation
a = bands.band_1.glcm;
Ng = size(a,1);

px = sum(a, 1);
py = sum(a, 2);
HX = -sum(px.*log2(px + eps));
HY = -sum(py.*log2(py + eps));
HXY = -sum(sum(a.*log2(a + eps)));

HXY1 = 0;
HXY2 = 0;
for i = 1:Ng
    for j = 1:Ng
        HXY1 = HXY1 + a(i,j) * log2(px(i)*py(j) + eps);
        HXY2 = HXY2 + px(i) * py(j) * log2(px(i)*py(j) + eps);
    end
end
HXY1 = -1*HXY1;
HXY2 = -1*HXY2;


imc1 = (HXY-HXY1)/max([HX, HY])
imc2 = sqrt(1-exp(-2.0*(HXY2-HXY)))

%% maximal correlation coefficient
a = bands.band_1.glcm;
px = sum(a, 1);
py = sum(a, 2);
Ng = size(a,1);
Q = zeros(size(a));

for i = 1:Ng
    for j = 1:Ng
        temp = 0
        for k = 1:Ng
            temp = temp + a(i,k)*a(j,k)/px(i)/py(k);
        end
        Q(i,j) = temp;
    end
end
qe = eig(Q);
mcc = sqrt(max(qe(qe<max(qe))))

