# -*- coding: utf-8 -*-
"""
/***************************************************************************
 BiomassEstimationUtilityToolbox
                                 A QGIS plugin
 This plugin lets you calculate easily several features from rasters to be used in forest biomass estimation.
                             -------------------
        begin                : 2016-05-23
        copyright            : (C) 2016 by Jenna Ritvanen
        email                : jenna.ritvanen@gmail.com
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load BiomassEstimationUtilityToolbox class from file BiomassEstimationUtilityToolbox.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .biomass_estimation_utility_toolbox import BiomassEstimationUtilityToolbox
    return BiomassEstimationUtilityToolbox(iface)
