# -*- coding: utf-8 -*-
#
# /***************************************************************************
#  BiomassEstimationUtilityToolbox
#                                  A QGIS plugin
#  This plugin lets you calculate easily several features
#  from rasters to be used in forest biomass estimation.
#     begin                : 2016-05-23
#     copyright            : (C) 2016 by Jenna Ritvanen
#     email                : jenna.ritvanen@gmail.com
#  ***************************************************************************/
#
# /***************************************************************************
#  *                                                                         *
#  *   This program is free software; you can redistribute it and/or modify  *
#  *   it under the terms of the GNU General Public License as published by  *
#  *   the Free Software Foundation; either version 2 of the License, or     *
#  *   (at your option) any later version.                                   *
#  *                                                                         *
#  ***************************************************************************/
#
#  This script allows running the toolbox from Python shell.

from __future__ import print_function
import argparse
import sys
import numpy as np
import file_operations
from feature_definitions import FEATURES
import feature_algorithms


def get_offset_from_str(str):
    """
    Helper function to checking arguments.
    """
    if str == '0':
        return 0
    elif str == '45':
        return np.pi / 4
    elif str == '90':
        return np.pi / 2
    elif str == '135':
        return 3 * np.pi / 4

def main():
    """
    Run toolbox from Python shell.
    """
    parser = argparse.ArgumentParser(description='Process satellite image and calculate features.')

    # Define possible arguments
    parser.add_argument('raster', action='store')
    parser.add_argument('-o', '--output', action='store', dest='mat_path', default='~/data.mat')
    parser.add_argument('-b', '--bands', action='store', dest='bands', type=int, nargs='+', required=True)
    parser.add_argument('-l', '--lidarPath', action='store', dest='lidar_path', required=True)
    parser.add_argument('-f', '--forestMask', action='store', dest='forest_mask_path', default='')
    parser.add_argument('--GLCMOffset', action='store', dest='offset', type=get_offset_from_str,
                        choices=['0', '45', '90', '135'], default='0')
    parser.add_argument('--GLCMDistance', action='store', dest='distance', type=int, default=1)
    parser.add_argument('--GLCMLevels', action='store', dest='levels', type=int, default=61, choices=range(0, 256))
    parser.add_argument('--GLCMSymmetric', action='store_true', dest='symmetric', help='Number of gray tone values for image to be scaled in (max. 255)')

    args = parser.parse_args()
    raster_path = args.raster
    mat_path = args.mat_path
    bands = args.bands
    lidar_path = args.lidar_path
    mask_path = args.forest_mask_path
    offset = args.offset
    distance = args.distance
    levels = args.levels
    symmetric = args.symmetric

    # Read the raster file
    error_msg, result = file_operations.resample_observations(raster_path, lidar_path, bands, mask_path, print)
    if str(error_msg):
        raise Exception(error_msg)

    # Run GLCM
    error_msg, result = file_operations.calculate_glcm(result, offset, distance, levels, symmetric, print)
    if str(error_msg):
        raise Exception(error_msg)

    # Run feature calculations
    for feat in FEATURES:
        for band, band_data in result['bands'].items():
            result['bands'][band][str(feat['variable'])] = feat['function'](result, band, print)
            # The feature name is cast as string (even though it's already string)
            # as a workaround to numpy issue https://github.com/numpy/numpy/issues/2407
    print(result)
    # Save the result as MAT file
    error_msg = file_operations.write_to_mat(result, mat_path)
    print(error_msg)


if __name__ == "__main__":
    main()
