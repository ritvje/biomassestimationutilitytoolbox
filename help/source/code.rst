Documentation for the software
******************************

Dependencies
############
The following Python modules are required for running the program:

  * os
  * sys
  * PyQt4
  * numpy
  * osgeo
  * scipy.io
  * shapely
  * skimage
  * struct

They need to be installed for the Python version that QGIS uses (usually 2.7).

The GDAL library is recommended to be 2.0.1 or higher.

For the command line interface also

  * argparse
  * __future__

are required. The command line interface requires Python 2.7 because of the __future__ module and the print function that is used through it, but it can also be replaced with the print function of Python 3.

file_operations.py
##################
This file includes all the functions that operate on raster, vector ad data files.

.. automodule:: file_operations
   :members:

feature_definitions.py
######################
This file lists all the definitions for the feature calculation in the FEATURES dictionary. The UIs will pick available features from this file.

The stricture of FEATURES is the following:

  * name: the name of the feature that is shown in GUI list; a string
  * variable: the name of the variable that holds the result of feature calculation in Matlab data; a string
  * function: link to the function used to calculate feature that UIs call; given with the help of feature_algorithms module, eg. fa.mean
  * description: the description of the feature that is shown in GUI when user clicks at the function name; a string

feature_algorithms.py
#####################
This file consists of functions to calculate features. All the functions have the same input variables so they can be called similarly from the UI and they should return single output value or array.

.. automodule:: feature_algorithms
   :members:

run_toolbox.py
##############
This file contains the command line interface. It is implemented through argparse module.

.. automodule:: run_toolbox
   :members:

Graphical User Interface
########################
The graphical user interface consists of files

  * biomass_estimation_utility_toolbox_dialog_base.ui
  * progress_dialog.ui
  * biomass_estimation_utility_toolbox_dialog.py
  * biomass_estimation_utility_toolbox.py

They were mostly built with QGIS Plugin Builder and Qt Designer.
