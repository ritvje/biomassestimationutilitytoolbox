How to add new features
***********************
New features are added through the feature_algorithms.py and feature_definitions.py files.

The function for calculating the feature is added to feature_algorithms.py. The function should have as input arguments

  * data: dictionary that should have the following structure:

    { 'bands': { band_number: { 'pixels': array, 'glcm': array } } }
  * band_number: label of the desired band in dictionary data
  * inform_progress: function that can be used to inform the user of the progress in time demanding calculations; by default None

The function should return the result of the calculation as a single return argument, either a number or an array.

The new feature is added to the feature_definitions.py file to the FEATURES dictionary where the UIs will be able to pick it up and display.

The stricture of FEATURES is the following:

  * name: the name of the feature that is shown in GUI list; a string
  * variable: the name of the variable that holds the result of feature calculation in Matlab data
  * function: link to the function used to calculate feature that UIs call; given with the help of feature_algorithms module, eg. fa.mean
  * description: the description of the feature that is shown in GUI when user clicks at the function name
