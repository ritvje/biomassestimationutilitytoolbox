# -*- coding: utf-8 -*-
#
# /***************************************************************************
#  BiomassEstimationUtilityToolbox
#                                  A QGIS plugin
#  This plugin lets you calculate easily several features
#  from rasters to be used in forest biomass estimation.
#     begin                : 2016-05-23
#     copyright            : (C) 2016 by Jenna Ritvanen
#     email                : jenna.ritvanen@gmail.com
#  ***************************************************************************/
#
# /***************************************************************************
#  *                                                                         *
#  *   This program is free software; you can redistribute it and/or modify  *
#  *   it under the terms of the GNU General Public License as published by  *
#  *   the Free Software Foundation; either version 2 of the License, or     *
#  *   (at your option) any later version.                                   *
#  *                                                                         *
#  ***************************************************************************/
#
#  This file includes algorithms for feature calculation.


import numpy as np


def mean(data, band_number, inform_progress=None):
    """
    Calculate the mean of the data (excluding NaN values) of band that has number of band_number.

    **Arguments**:

        data: dictionary that should have the following structure:

            { 'bands': { band_number: { 'pixels': array, 'glcm': array } } }

        band_number: label of the desired band in dictionary data

        inform_progress: function that can be used to inform the user of the progress in time demanding calculations; by default None

    **Returns**:

        mean: mean of the data
    """
    array = data['bands'][band_number]['pixels']
    remove_nan_array = np.ma.masked_array(array, np.isnan(array))
    return np.mean(remove_nan_array)


def sum(data, band_number, inform_progress=None):
    """
    Calculate thesum of the data of band that has number of band_number.

    **Arguments**:

        data: dictionary that should have the following structure:

            { 'bands': { band_number: { 'pixels': array, 'glcm': array } } }

        band_number: label of the desired band in dictionary data

        inform_progress: function that can be used to inform the user of the progress in time demanding calculations; by default None

    **Returns**:

        sum: sum of the data
    """
    array = data['bands'][band_number]['pixels']
    return np.nansum(array)


def asm(data, band_number, inform_progress=None):
    """
    Calculate the angular second moment of band that has number of band_number.

    **Arguments**:

        data: dictionary that should have the following structure:

            { 'bands': { band_number: { 'pixels': array, 'glcm': array } } }

        band_number: label of the desired band in dictionary data

        inform_progress: function that can be used to inform the user of the progress in time demanding calculations; by default None

    **Returns**:

        asm: angular second moment of the data
    """
    array = data['bands'][band_number]['glcm']
    sum = 0
    for p in np.nditer(array):
        sum += p ** 2
    return sum


def contrast(data, band_number, inform_progress=None):
    """
    Calculate the contrast of band that has number of band_number.

    **Arguments**:

        data: dictionary that should have the following structure:

            { 'bands': { band_number: { 'pixels': array, 'glcm': array } } }

        band_number: label of the desired band in dictionary data

        inform_progress: function that can be used to inform the user of the progress in time demanding calculations; by default None

    **Returns**:

        contrast:  contrast of the data
    """
    array = data['bands'][band_number]['glcm']
    Ng = data['glcm_levels'] + 1
    sum = 0
    for n in range(0, Ng):
        temp_sum = 0
        for i in range(1, Ng + 1):
            for j in range(1, Ng + 1):
                if abs(i - j) == n:
                    temp_sum += array[i - 1][j - 1]
        sum += n ** 2 * temp_sum
    return sum


def correlation(data, band_number, inform_progress=None):
    """
    Calculate the correlation of band that has number of band_number.

    **Arguments**:

        data: dictionary that should have the following structure:

            { 'bands': { band_number: { 'pixels': array, 'glcm': array } } }

        band_number: label of the desired band in dictionary data

        inform_progress: function that can be used to inform the user of the progress in time demanding calculations; by default None

    **Returns**:

        correlation: correlation of the data
    """
    array = data['bands'][band_number]['glcm']
    px = np.sum(array, axis=0)
    py = np.sum(array, axis=1)
    std_dev_x = np.std(px, dtype=np.float64)
    std_dev_y = np.std(py, dtype=np.float64)
    mean_x = np.mean(px, dtype=np.float64)
    mean_y = np.mean(py, dtype=np.float64)
    sum = 0
    for i in range(0, data['glcm_levels'] + 1):
        for j in range(0, data['glcm_levels'] + 1):
            sum += array[i][j] * (i + 1) * (j + 1)
    return (sum - mean_x * mean_y) / (std_dev_x * std_dev_y)


def ssq(data, band_number, inform_progress=None):
    """
    Calculate the sum of squares (variance) of the data of band that has number of band_number.

    **Arguments**:

        data: dictionary that should have the following structure:

            { 'bands': { band_number: { 'pixels': array, 'glcm': array } } }

        band_number: label of the desired band in dictionary data

        inform_progress: function that can be used to inform the user of the progress in time demanding calculations; by default None

    **Returns**:

        ssq: sum of square (variance) of the data
    """
    array = data['bands'][band_number]['glcm']
    mean = np.mean(array, dtype=np.float64)
    sum = 0
    for i in range(0, data['glcm_levels'] + 1):
        for j in range(0, data['glcm_levels'] + 1):
            sum += (i + 1 - mean) ** 2 * array[i][j]
    return sum


def inv_diff_moment(data, band_number, inform_progress=None):
    """
    Calculate the inverse difference moment of the data of band that has number of band_number.

    **Arguments**:

        data: dictionary that should have the following structure:

            { 'bands': { band_number: { 'pixels': array, 'glcm': array } } }

        band_number: label of the desired band in dictionary data

        inform_progress: function that can be used to inform the user of the progress in time demanding calculations; by default None

    **Returns**:

        inv_diff_moment: the inverse difference moment of the data
    """
    array = data['bands'][band_number]['glcm']
    sum = 0
    for i in range(0, data['glcm_levels'] + 1):
        for j in range(0, data['glcm_levels'] + 1):
            sum += array[i][j] / (1 + ((i + 1) - (j + 1)) ** 2)
    return sum


def sum_avg(data, band_number, inform_progress=None):
    """
    Calculate the sum average of the data of band that has number of band_number.

    **Arguments**:

        data: dictionary that should have the following structure:

            { 'bands': { band_number: { 'pixels': array, 'glcm': array } } }

        band_number: label of the desired band in dictionary data

        inform_progress: function that can be used to inform the user of the progress in time demanding calculations; by default None

    **Returns**:

        sum_avg: sum average of the data
    """
    array = data['bands'][band_number]['glcm']
    sum = 0
    for k in range(2, 2 * (data['glcm_levels'] + 1) + 1):
        sum += k * __pxplusy(array, data['glcm_levels'] + 1, k)
    return sum


def sum_var(data, band_number, inform_progress=None):
    """
    Calculate the sum variance of the data of band that has number of band_number.

    **Arguments**:

        data: dictionary that should have the following structure:

            { 'bands': { band_number: { 'pixels': array, 'glcm': array } } }

        band_number: label of the desired band in dictionary data

        inform_progress: function that can be used to inform the user of the progress in time demanding calculations; by default None

    **Returns**:

        sum_var: sum variance of the data
    """
    array = data['bands'][band_number]['glcm']
    sum = 0
    f8 = sum_entropy(data, band_number, inform_progress)
    for k in range(2, 2 * (data['glcm_levels'] + 1) + 1):
        sum += (k - f8) ** 2 * __pxplusy(array, data['glcm_levels'] + 1, k)
    return sum


def sum_entropy(data, band_number, inform_progress=None):
    """
    Calculate the sum entropy of the data of band that has number of band_number.

    **Arguments**:

        data: dictionary that should have the following structure:

            { 'bands': { band_number: { 'pixels': array, 'glcm': array } } }

        band_number: label of the desired band in dictionary data

        inform_progress: function that can be used to inform the user of the progress in time demanding calculations; by default None

    **Returns**:

        sum_entropy: sum entropy of the data
    """
    array = data['bands'][band_number]['glcm']
    sum = 0
    for k in range(2, 2 * (data['glcm_levels'] + 1) + 1):
        tmp = __pxplusy(array, data['glcm_levels'] + 1, k)
        sum += tmp * np.log2(tmp + np.finfo(np.float64).eps)
    return (-1) * sum


def entropy(data, band_number, inform_progress=None):
    """
    Calculate the entropy of the data of band that has number of band_number.

    **Arguments**:

        data: dictionary that should have the following structure:

            { 'bands': { band_number: { 'pixels': array, 'glcm': array } } }

        band_number: label of the desired band in dictionary data

        inform_progress: function that can be used to inform the user of the progress in time demanding calculations; by default None

    **Returns**:

        entropy: entropy of the data
    """
    array = data['bands'][band_number]['glcm']
    sum = 0
    for p in np.nditer(array):
        sum += p * np.log2(p + np.finfo(np.float64).eps)
    return (-1) * sum


def diff_var(data, band_number, inform_progress=None):
    """
    Calculate the difference variance of the data of band that has number of band_number.

    **Arguments**:

        data: dictionary that should have the following structure:

            { 'bands': { band_number: { 'pixels': array, 'glcm': array } } }

        band_number: label of the desired band in dictionary data

        inform_progress: function that can be used to inform the user of the progress in time demanding calculations; by default None

    **Returns**:

        diff_var: difference variance of the data
    """
    array = data['bands'][band_number]['glcm']
    sum = 0
    f11 = diff_entropy(data, band_number, inform_progress)
    for k in range(0, data['glcm_levels'] + 1):
        sum += (k - f11) ** 2 * __pxminusy(array, data['glcm_levels'] + 1, k)
    return sum


def diff_entropy(data, band_number, inform_progress=None):
    """
    Calculate the difference entropy of the data of band that has number of band_number.

    **Arguments**:

        data: dictionary that should have the following structure:

            { 'bands': { band_number: { 'pixels': array, 'glcm': array } } }

        band_number: label of the desired band in dictionary data

        inform_progress: function that can be used to inform the user of the progress in time demanding calculations; by default None

    **Returns**:

        diff_entropy: difference entropy of the data
    """
    array = data['bands'][band_number]['glcm']
    sum = 0
    for k in range(0, data['glcm_levels'] + 1):
        tmp = __pxminusy(array, data['glcm_levels'] + 1, k)
        sum += tmp * np.log2(tmp + np.finfo(np.float64).eps)
    return (-1) * sum


def info_meas_corr_1(data, band_number, inform_progress=None):
    """
    Calculate the first information measure of correlation of the data (excluding NaN values) of band that has number of band_number.

    **Arguments**:

        data: dictionary that should have the following structure:

            { 'bands': { band_number: { 'pixels': array, 'glcm': array } } }

        band_number: label of the desired band in dictionary data

        inform_progress: function that can be used to inform the user of the progress in time demanding calculations; by default None

    **Returns**:

        info_meas_corr_1: first information measure of correlation of the data
    """
    array = data['bands'][band_number]['glcm']
    px = np.sum(array, axis=0)
    py = np.sum(array, axis=1)
    Ng = data['glcm_levels'] + 1

    HX = (-1) * np.sum(np.multiply(px, np.log2(px + np.finfo(np.float64).eps)))
    HY = (-1) * np.sum(np.multiply(py, np.log2(py + np.finfo(np.float64).eps)))
    HXY = (-1) * np.sum(np.multiply(array, np.log2(array + np.finfo(np.float64).eps)))

    HXY1 = 0
    for i in range(1, Ng + 1):
        for j in range(1, Ng + 1):
            HXY1 += array[i - 1][j - 1] * np.log2(px[i - 1] * py[j - 1] + np.finfo(np.float64).eps)
    HXY1 = (-1) * HXY1

    f12 = (HXY - HXY1) / np.maximum(HX, HY)
    return f12


def info_meas_corr_2(data, band_number, inform_progress=None):
    """
    Calculate the second information measure of correlation of the data (excluding NaN values) of band that has number of band_number.

    **Arguments**:

        data: dictionary that should have the following structure:

            { 'bands': { band_number: { 'pixels': array, 'glcm': array } } }

        band_number: label of the desired band in dictionary data

        inform_progress: function that can be used to inform the user of the progress in time demanding calculations; by default None

    **Returns**:

        info_meas_corr_2: second information measure of correlation of the data
    """
    array = data['bands'][band_number]['glcm']
    px = np.sum(array, axis=0)
    py = np.sum(array, axis=1)
    Ng = data['glcm_levels'] + 1

    HXY = (-1) * np.sum(np.multiply(array, np.log2(array + np.finfo(np.float64).eps)))

    HXY2 = 0
    for i in range(1, Ng + 1):
        for j in range(1, Ng + 1):
            HXY2 += px[i - 1] * py[j - 1] * np.log2(px[i - 1] * py[j - 1] + np.finfo(np.float64).eps)
    HXY2 = (-1) * HXY2

    f13 = np.sqrt(1 - np.exp(-2.0 * (HXY2 - HXY)))
    return f13


def max_corr_coeff(data, band_number, inform_progress=None):
    """
    Calculate the smaximal correlation coefficient of the data (excluding NaN values) of band that has number of band_number.

    **Arguments**:

        data: dictionary that should have the following structure:

            { 'bands': { band_number: { 'pixels': array, 'glcm': array } } }

        band_number: label of the desired band in dictionary data

        inform_progress: function that can be used to inform the user of the progress in time demanding calculations; by default None

    **Returns**:

        max_corr_coeff: maximal correlation coefficient of the data
    """
    array = data['bands'][band_number]['glcm']
    array = np.ma.masked_array(array, np.isnan(array))
    px = np.sum(array, axis=0)
    py = np.sum(array, axis=1)
    Ng = data['glcm_levels'] + 1

    Q = np.zeros((Ng, Ng))

    for i in range(1, Ng + 1):
        for j in range(1, Ng + 1):
            tmp = 0
            for k in range(1, Ng + 1):
                if (px[i - 1]) and (py[k - 1]):
                    tmp += (array[i - 1][k - 1] * array[j - 1][k - 1]) / (px[i - 1] * py[k - 1])
                else:
                    tmp += 0
            Q[i - 1][j - 1] = tmp

    qe, qv = np.linalg.eig(Q)
    qe.sort()
    mcc = np.sqrt(qe[-2])
    return mcc


def __pxplusy(array, Ng, k):
    """
    Helper function that calculates sum of array cells for the cells
    where the sum of row and column indices equals k.

    **Arguments**:

        array:  two-dimensional square array for which the sum is calculated

        Ng: the width/height of array

        k: the required value for the sum of row and column indices

    **Returns**:

        sum: the calculated sum
    """
    sum = 0
    for i in range(1, Ng + 1):
        for j in range(1, Ng + 1):
            if i + j == k:
                sum += array[i - 1][j - 1]
    return sum


def __pxminusy(array, Ng, k):
    """
    Helper function that calculates sum of array cells for the cells
    where the absolute value of difference of row and column indices equals k.

    **Arguments**:

        array:  two-dimensional square array for which the sum is calculated

        Ng: the width/height of array

        k: the required value for the absolute value of difference of row and column indices

    **Returns**:

        sum: the calculated sum
    """
    sum = 0
    for i in range(1, Ng + 1):
        for j in range(1, Ng + 1):
            if abs(i - j) == k:
                sum += array[i - 1][j - 1]
    return sum
