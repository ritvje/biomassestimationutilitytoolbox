# -*- coding: utf-8 -*-
#
# /***************************************************************************
#  BiomassEstimationUtilityToolboxDialog
#                                  A QGIS plugin
#  This plugin lets you calculate easily several features from rasters to be used in forest biomass estimation.
#                              -------------------
#         begin                : 2016-05-23
#         git sha              : $Format:%H$
#         copyright            : (C) 2016 by Jenna Ritvanen
#         email                : jenna.ritvanen@gmail.com
#  ***************************************************************************/
#
# /***************************************************************************
#  *                                                                         *
#  *   This program is free software; you can redistribute it and/or modify  *
#  *   it under the terms of the GNU General Public License as published by  *
#  *   the Free Software Foundation; either version 2 of the License, or     *
#  *   (at your option) any later version.                                   *
#  *                                                                         *
#  ***************************************************************************/


import os

from PyQt4 import QtGui, uic

import file_operations
from feature_definitions import FEATURES

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'biomass_estimation_utility_toolbox_dialog_base.ui'))
PROGRESS_DIALOG_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'progress_dialog.ui'))

WINDOW_TITLE = 'Biomass Estimation Utility Toolbox'


class ProgressDialog(QtGui.QDialog, PROGRESS_DIALOG_CLASS):
    def __init__(self, parent=None):
        super(ProgressDialog, self).__init__(parent)
        self.setupUi(self)

    def show_message(self, text):
        self.text_edit_info.insertPlainText(text + '\n')
        self.show()


class BiomassEstimationUtilityToolboxDialog(QtGui.QDialog, FORM_CLASS):
    def __init__(self, parent=None, iface=None):
        """Constructor."""
        super(BiomassEstimationUtilityToolboxDialog, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.iface = iface
        self.setupUi(self)

        self.progress_dialog = ProgressDialog(self)
        self.progress_dialog.setModal(False)

        # Populate list view with features
        self.populate_list_view_features()
        self.list_view_features.selectionModel().currentChanged.connect(self.set_feature_description)
        self.check_all_features.stateChanged.connect(self.select_all_features)

        # Bind all buttons etc. to their actions
        self.btn_browse_raster.clicked.connect(self.browse_raster_file)
        self.btn_browse_base.clicked.connect(self.browse_base_file)
        self.btn_browse_mat.clicked.connect(self.browse_mat_file)
        self.btn_browse_mask.clicked.connect(self.browse_mask_file)
        self.btn_add_bands.clicked.connect(self.populate_list_view_chosen_bands)
        self.btn_remove_bands.clicked.connect(self.remove_bands_from_list_view_chosen_bands)
        self.check_all_bands.stateChanged.connect(self.select_all_bands)

    def browse_raster_file(self):
        """
        Opens file dialog when button is pressed and enables search on raster files.
        Also calls function to populate list view with all available bands.
        """
        filepath = QtGui.QFileDialog.getOpenFileName(self, self.tr('Choose raster file'), '/home', self.tr('Raster files (*.tif *.TIF *.tiff *.TIFF)'))

        if filepath:
            self.line_raster_path.clear()
            self.line_raster_path.insert(filepath)
            self.populate_list_view_all_bands(filepath)

    def browse_base_file(self):
        """
        Opens file dialog when button is pressed and enables search on raster files.
        """
        filepath = QtGui.QFileDialog.getOpenFileName(self, self.tr('Choose raster file'), '/home', '')

        if filepath:
            self.line_base_path.clear()
            self.line_base_path.insert(filepath)

    def browse_mat_file(self):
        """
        Opens file dialog when button is pressed and enables search on mat-files.
        Checks that file has correct suffix.
        """
        file_dialog = QtGui.QFileDialog()
        file_dialog.setConfirmOverwrite(False)
        filepath = file_dialog.getSaveFileName(self, self.tr('Choose MAT-file'), '/home', self.tr('MAT-file (*.mat)'))

        # Make sure file has proper suffix
        if not filepath.upper().endswith(".MAT"):
            filepath = filepath + ".mat"
        if filepath:
            self.line_mat_path.clear()
            self.line_mat_path.insert(filepath)

    def browse_mask_file(self):
        """
        Opens file dialog when button is pressed and enables search on shapefiles.
        """
        filepath = QtGui.QFileDialog.getOpenFileName(self, self.tr('Choose forest mask file'), '/home', '')

        if filepath:
            self.line_mask_path.clear()
            self.line_mask_path.insert(filepath)

    def populate_list_view_all_bands(self, path):
        """
        Gets all bands from given raster and populates list_view_all_bands with them.
        """
        model = QtGui.QStandardItemModel(self.list_view_all_bands)
        # Remove all items from list_view_chosen_bands if any exist so it doesn't show old bands
        # list_view_all_bands is cleared anyway
        if self.list_view_chosen_bands.model():
            self.list_view_chosen_bands.model().clear()

        error_msg, band_info = file_operations.get_band_info_of_raster(path)

        if error_msg:
            return self.handle_error(error_msg)

        for band in band_info:
            item = QtGui.QStandardItem()
            # The band number will be stored in role 32 and unit_type in role 33
            # Thus the text of each item can be later changed easily
            item.setData(band['number'], 32)
            item.setData(band['unit_type'], 33)
            item.setText(str(band['number']))
            item.setCheckable(True)
            model.appendRow(item)
        self.list_view_all_bands.setModel(model)

    def populate_list_view_chosen_bands(self):
        """
        Populates list_view_chosen_bands with the bands that are selected on list_view_all_bands.
        """
        model_all_bands = self.list_view_all_bands.model()
        model_chosen_bands = QtGui.QStandardItemModel(self.list_view_chosen_bands)
        if model_all_bands is not None:
            i = 0
            while model_all_bands.item(i):
                if model_all_bands.item(i).checkState() == 2:  # the item is selected
                    # copy the item to list_view_chosen_bands
                    item = model_all_bands.item(i).clone()
                    item.setCheckState(0)
                    model_chosen_bands.appendRow(item)
                model_all_bands.item(i).setCheckState(0)
                i += 1
            self.list_view_chosen_bands.setModel(model_chosen_bands)
            self.check_all_bands.setCheckState(0)

    def remove_bands_from_list_view_chosen_bands(self):
        """
        Removes selected items from list_view_chosen_bands.
        """
        model = self.list_view_chosen_bands.model()
        if model is not None:
            rows_to_remove = []
            i = 0
            while model.item(i):
                if model.item(i).checkState() == 2:
                    rows_to_remove.append(i)
                i += 1
            for row in reversed(rows_to_remove):  # indexes on list update automatically, so have to first
                # remove largest index and so on
                self.list_view_chosen_bands.model().removeRow(row)

    def select_all_bands(self, state):
        """
        Sets the state of all items in list_view_all_bands to be same as state.
        If no items are available, sets checkbox to not selected.

        Arguments:
        state -- QCheckState of check_all_bands checkbox
        """
        model = self.list_view_all_bands.model()
        if model is not None:
            i = 0
            while model.item(i):
                model.item(i).setCheckState(state)
                i += 1
        else:
            self.check_all_bands.setCheckState(0)

    def get_chosen_bands(self):
        """
        Return a list of chosen band numbers.

        Returns:
        bands -- list of band numbers as integers
        """
        bands = []
        model = self.list_view_chosen_bands.model()
        if model is not None:
            i = 0
            while model.item(i):
                bands.append(int(model.item(i).data(32)))
                i += 1
        return bands

    def populate_list_view_features(self):
        """
        Takes the functions defined in feature_algorithms and populates list_view_features with them.
        """
        model = QtGui.QStandardItemModel(self.list_view_features)
        for feature in FEATURES:
            item = QtGui.QStandardItem()
            item.setData(feature['name'], 32)
            item.setData(feature['function'], 33)
            item.setData(feature['description'], 34)
            item.setData(feature['variable'], 35)
            item.setText(feature['name'])
            item.setCheckable(True)
            model.appendRow(item)
        self.list_view_features.setModel(model)

    def set_feature_description(self, selected_item, deselected_item):
        """
        Function that bound to list_view_features selectionChanged. Sets the selected feature description in text_edit_info.

        Arguments:
        selected_item -- index for the currently selected item
        deselected_item -- index for the item that was selected previously
        """
        model = self.list_view_features.model()
        feature = model.itemFromIndex(selected_item)
        self.text_edit_info.clear()
        self.text_edit_info.insertPlainText(feature.data(34))

    def select_all_features(self, state):
        """
        Sets the state of all items in list_view_features to be same as state.
        If no items are available, sets checkbox to not selected.

        Arguments:
        state -- QCheckState of check_all_bands checkbox
        """
        model = self.list_view_features.model()
        if model is not None:
            i = 0
            while model.item(i):
                model.item(i).setCheckState(state)
                i += 1
        else:
            self.check_all_bands.setCheckState(0)

    def get_chosen_features(self):
        """
        Returns the chosen features in list

        Returns:
        features -- list of dictionaries: {'name', 'function'}
        """
        model = self.list_view_features.model()
        features = []
        if model is not None:
            i = 0
            while model.item(i):
                if model.item(i).checkState() == 2:
                    dict = {}
                    dict['name'] = model.item(i).data(32)
                    dict['function'] = model.item(i).data(33)
                    dict['variable'] = model.item(i).data(35)
                    features.append(dict)
                i += 1
        return features

    def run_chosen_features(self, data):
        """
        Executes the chosen features on data.

        Arguments:
        data -- result dictionary from file_operations.resample_observations

        Returns:
        data -- the result of calculations appended to given data
        """
        features = self.get_chosen_features()
        if features:  # some features were chosen
            error_msg, data = self.run_glcm_calculations(data)
            if not error_msg:
                for feat in features:
                    for band, band_data in data['bands'].items():
                        data['bands'][band][str(feat['variable'])] = feat['function'](data, band, self.progress_dialog.show_message)
                        # The feature name is cast as string (even though it's already string)
                        # as a workaround to numpy issue https://github.com/numpy/numpy/issues/2407
            else:
                return self.handle_error(error_msg)
        return data

    def run_glcm_calculations(self, data):
        """
        Calls a function from file_operations to calculate GLCM for the band data.

        Arguments:
        data -- result dictionary from file_operations.resample_observations

        Returns:
        error_msg -- possible error messages; empty string if none happened
        data -- argument data appended with glcms
        """
        # Make sure offset is given correctly
        offset = self.combo_offset.currentText()
        if offset == '0 degrees':
            angle = 0
        elif offset == '45 degrees':
            angle = np.pi / 4
        elif offset == '90 degrees':
            angle = np.pi / 2
        elif offset == '135 degrees':
            angle = 3 * np.pi / 4
        else:
            self.handle_error('GLCM offset couldn\'t be interpreted!')

        return file_operations.calculate_glcm(data, angle, self.spin_box_distance.value(), self.spin_box_levels.value(), self.check_symmetric.isChecked(), self.progress_dialog.show_message)

    def accept(self):
        """
        Function that is executed when user clicks OK.
        """
        # Check that all mandatory file fields are filled
        fields = [
            (self.line_base_path.text(), 'Please choose a raster to be used as base raster.'),
            (self.line_raster_path.text(), 'Please choose a raster to be resampled. '),
            (self.line_mat_path.text(), 'Please choose a mat file for results. ')
        ]
        for content, msg in fields:
            if not self.line_edit_is_valid(content):
                return self.handle_error(msg)

        bands = self.get_chosen_bands()
        if len(bands) == 0:
            return self.handle_error('Please choose at least one band to be resampled!')

        error_msg, result = file_operations.resample_observations(self.line_raster_path.text(), self.line_base_path.text(), bands, self.line_mask_path.text(), self.progress_dialog.show_message)
        if error_msg:
            return self.handle_error(error_msg)

        result = self.run_chosen_features(result)
        error_msg = file_operations.write_to_mat(result, self.line_mat_path.text())
        if error_msg:
            return self.handle_error(error_msg)
        self.inform_user('Features calculated successfully!')

    def line_edit_is_valid(self, content):
        """
        Checks that line edit content is valid.
        """
        if not str(content):
            return False
        return True

    def handle_error(self, error):
        """
        Function that prints error to the user.

        Arguments:
        error -- error message that is printed
        """
        QtGui.QMessageBox.warning(self, self.tr(WINDOW_TITLE), self.tr(error))
        return

    def inform_user(self, message):
        """
        Function that prints message to the user.

        Arguments:
        message -- message that is printed
        """
        QtGui.QMessageBox.information(self, self.tr(WINDOW_TITLE), self.tr(message))
        return
