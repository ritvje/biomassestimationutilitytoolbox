# -*- coding: utf-8 -*-
#
# /***************************************************************************
#  BiomassEstimationUtilityToolbox
#                                  A QGIS plugin
#  This plugin lets you calculate easily several features
#  from rasters to be used in forest biomass estimation.
#     begin                : 2016-05-23
#     copyright            : (C) 2016 by Jenna Ritvanen
#     email                : jenna.ritvanen@gmail.com
#  ***************************************************************************/
#
# /***************************************************************************
#  *                                                                         *
#  *   This program is free software; you can redistribute it and/or modify  *
#  *   it under the terms of the GNU General Public License as published by  *
#  *   the Free Software Foundation; either version 2 of the License, or     *
#  *   (at your option) any later version.                                   *
#  *                                                                         *
#  ***************************************************************************/
#
#  This file includes definitions for feature calculation.


import feature_algorithms as fa


FEATURES = [{'name': 'Mean', 'variable': 'mean', 'function': fa.mean, 'description': 'Mean of the pixels'},
            {'name': 'Sum', 'variable': 'sum', 'function': fa.sum, 'description': 'Total sum of the pixels'},
            {'name': 'Angular Second Moment', 'variable': 'angular_second_motion', 'function': fa.asm, 'description': 'A measure of homogeneity of image'},
            {'name': 'Contrast', 'variable': 'contrast', 'function': fa.contrast, 'description': 'Measure of the local variations in image'},
            {'name': 'Correlation', 'variable': 'correlation', 'function': fa.correlation, 'description': 'Measure of gray-tone linear-dependencies in image'},
            {'name': 'Sum of squares (variance)', 'variable': 'variance', 'function': fa.ssq, 'description': 'Sum of squares'},
            {'name': 'Inverse Difference Moment', 'variable': 'inv_diff_moment', 'function': fa.inv_diff_moment, 'description': 'Inverse Difference Moment'},
            {'name': 'Sum Average', 'variable': 'sum_average', 'function': fa.sum_avg, 'description': 'Sum Average'},
            {'name': 'Sum Variance', 'variable': 'sum_variance', 'function': fa.sum_var, 'description': 'Sum Variance'},
            {'name': 'Sum Entropy', 'variable': 'sum_entropy', 'function': fa.sum_entropy, 'description': 'Sum Entropy'},
            {'name': 'Entropy', 'variable': 'entropy', 'function': fa.entropy, 'description': 'Entropy'},
            {'name': 'Difference Variance', 'variable': 'diff_variance', 'function': fa.diff_var, 'description': 'Difference Variance'},
            {'name': 'Difference Entropy', 'variable': 'diff_entropy', 'function': fa.diff_entropy, 'description': 'Difference Entropy'},
            {'name': 'Information Measure of Correlation 1', 'variable': 'info_meas_corr_1', 'function': fa.info_meas_corr_1, 'description': 'First Information Measure of Correlation'},
            {'name': 'Information Measure of Correlation 2', 'variable': 'info_meas_corr_2', 'function': fa.info_meas_corr_2, 'description': 'Second Information Measure of Correlation'},
            {'name': 'Maximal Correlation Coefficient', 'variable': 'max_corr_coeff', 'function': fa.max_corr_coeff, 'description': 'Maximal Correlation Coefficient'}]
