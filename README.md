# Biomass Estimation Utility Toolbox

This repository contains a QGIS plugin intended to provide easy way to calculate features from satellite images to be used in forest biomass estimation.

The plugin is developed as a part of my Bachelor's thesis in Lappeenranta University of Technology, School of Engineering Science, degree program in Computational Engineering.

## Dependencies

The plugin is developed for QGIS 2.8.9, but requires in general any QGIS version >=2.0. QGIS must have Python-bindings installed. In addition the following Python modules (for the version QGIS uses) must be installed:
	`osgeo`
	`scipy`
	`scikit-image` `shapely`



## Installation

The plugin can be deployed using
	`make deploy`
assuming GNU Make is installed. After deployment, (re-)start QGIS and make sure the plugin is enabled in QGIS Plugins -> Manage and Install Plugins -> Installed.

## Developing

The plugin tries to follow PEP8, so any additions should be written according to PEP8.