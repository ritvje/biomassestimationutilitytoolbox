# -*- coding: utf-8 -*-
#
# /***************************************************************************
#  BiomassEstimationUtilityToolbox
#                                  A QGIS plugin
#  This plugin lets you calculate easily several features
#  from rasters to be used in forest biomass estimation.
#     begin                : 2016-05-23
#     copyright            : (C) 2016 by Jenna Ritvanen
#     email                : jenna.ritvanen@gmail.com
#  ***************************************************************************/
#
# /***************************************************************************
#  *                                                                         *
#  *   This program is free software; you can redistribute it and/or modify  *
#  *   it under the terms of the GNU General Public License as published by  *
#  *   the Free Software Foundation; either version 2 of the License, or     *
#  *   (at your option) any later version.                                   *
#  *                                                                         *
#  ***************************************************************************/
#
#  This file includes necessary file operations using gdal and ogr from osgeo.
#  They should not be imported in any other files.


from osgeo import gdal, ogr
from gdalconst import *
import sys
import numpy as np
import scipy.io as sio
from shapely.wkb import loads
from skimage.feature import greycomatrix
from shapely.geometry import Polygon
import struct

# Enable exceptions
gdal.UseExceptions()


def get_band_info_of_raster(path):
    """
    Opens raster and return info of its bands in list of dictionaries

    **Arguments**:

        path: absolute path to raster

    **Returns**:

        error_msg: possible error messages; an empty string if no errors

        bands: list of dictionaries of band info: {'number': int, 'unit_type': str}
    """
    bands = []
    error_msg = ''
    try:
        # Open raster file to get bands
        dataset = gdal.Open(path, GA_ReadOnly)
    except RuntimeError as e:
        error_msg += 'The following error happened while opening file:\n' + str(e)
        return error_msg, None

    band_count = dataset.RasterCount
    for band_number in range(1, band_count + 1):
        band = dataset.GetRasterBand(band_number)
        band_info = {}
        band_info['number'] = band.GetBand()
        band_info['unit_type'] = band.GetUnitType()
        bands.append(band_info)
    dataset = None
    return error_msg, bands


def resample_observations(observations_path, base_path, bands, mask_path, inform_progress):
    """
    Creates an array of the same size as raster at base_path and extracts values from observations_path raster to it. Values are resampled so, that in the end base_path raster and new array have pixel values at matching locations with the same resolution, geo transformation and projection.

    **Arguments**:

        observations_path: absolute path to raster that values are taken from

        base_path: absolute path to raster that is used as a base for new       raster. Only first band is used.

        bands: list of band numbers that are to be resampled

    **Returns**:

        error_msg: possible error messages; empty string if none happened

        result_arrays: dictionary that has the resampled data
    """
    error_msg = ''

    mask_exists = False
    if str(mask_path):
        mask_exists = True

    try:
        observations_dataset = gdal.Open(observations_path, GA_ReadOnly)
        base_dataset = gdal.Open(base_path, GA_ReadOnly)
        if mask_exists:
            inform_progress('Reading forest mask...')
            sys.stdout.flush()
            mask_data = ogr.Open(mask_path, GA_ReadOnly)
            layer = mask_data.GetLayer()
            mask_obj = []
            for feat in layer:
                inform_progress('Reading forest mask part')
                obj = loads(feat.GetGeometryRef().ExportToWkb(), hex=False)
                mask_obj.append(obj)
    except RuntimeError as e:
        error_msg += 'The following error happened while opening file:\n' + str(e)
        return error_msg, None

    observations_gt = observations_dataset.GetGeoTransform()
    base_gt = base_dataset.GetGeoTransform()

    base_band = base_dataset.GetRasterBand(1)
    no_data_value = base_band.GetNoDataValue()
    width = base_dataset.RasterXSize
    height = base_dataset.RasterYSize

    # For reading the pixel values
    fmttypes = {'Byte': 'B', 'UInt16': 'H', 'Int16': 'h', 'UInt32': 'I', 'Int32 ': 'i', 'Float32': 'f', 'Float64': 'd'}

    observations_bands = []
    for b in bands:
        observations_bands.append(observations_dataset.GetRasterBand(b))

    result_arrays = {}
    # Add necessary information to turn arrays into rasters
    result_arrays['GeoTransform'] = base_gt
    result_arrays['Projection'] = base_dataset.GetProjection()
    result_arrays['Format'] = 'GTiff'
    result_arrays['width'] = width
    result_arrays['height'] = height
    result_arrays['bands'] = {}

    last_index = 0  # to store the idx of the Multipolygon the last pixel was in

    # Let's first create the arrays in which the resampled data is placed
    for band in observations_bands:
        array = np.empty([height, width])
        array.fill(0)
        result_arrays['bands']['band_' + str(band.GetBand())] = {}
        result_arrays['bands']['band_' + str(band.GetBand())]['pixels'] = array

    base_pixels = base_band.ReadAsArray(0, 0, width, height)
    for i in range(height):
        for j in range(width):
            if base_pixels[i][j] != no_data_value:
                # calculate pixel corner coordinates
                left_x_coord = base_gt[0] + j * base_gt[1]
                top_y_coord = base_gt[3] + i * base_gt[5]
                right_x_coord = base_gt[0] + (j + 1) * base_gt[1]
                bottom_y_coord = base_gt[3] + (i + 1) * base_gt[5]

                # coordinates in pixel coordinates of obsv raster
                left_x = (left_x_coord - observations_gt[0]) / observations_gt[1]
                top_y = (top_y_coord - observations_gt[3]) / observations_gt[5]
                right_x = (right_x_coord - observations_gt[0]) / observations_gt[1]
                bottom_y = (bottom_y_coord - observations_gt[3]) / observations_gt[5]

                # integer values of corner x & y (to get the pixels of band that are on the pixel)
                left_x_int = int(left_x)
                right_x_int = int(right_x)
                top_y_int = int(top_y)
                bottom_y_int = int(bottom_y)

                if mask_exists:
                    # Check that pixel is fully inside the mask
                    pixel = Polygon([
                        (left_x_coord, top_y_coord),
                        (right_x_coord, top_y_coord),
                        (right_x_coord, bottom_y_coord),
                        (left_x_coord, bottom_y_coord),
                        (left_x_coord, top_y_coord)
                    ])
                    error_msg, inside, last_index = pixel_inside_mask(pixel, mask_obj, last_index)
                    inform_progress('Pixel ({0}, {1}) is inside mask: {2}'.format(i, j, inside))
                    if not inside:
                        continue

                for band in observations_bands:
                    # Here are two options for reading the pixel values.
                    # Option 1: ready function from GDAL. If any errors happen, error messages vanish and the result is None
                    observations_pixels = band.ReadAsArray(left_x_int, top_y_int, right_x_int - left_x_int + 1, bottom_y_int - top_y_int + 1)

                    # Option 2: same things as in function in option 1.
                    # Errors are raised if any occur and the program will crash
                    # scanarray = band.ReadRaster(left_x_int, top_y_int, right_x_int - left_x_int + 1, bottom_y_int - top_y_int + 1, right_x_int - left_x_int + 1, bottom_y_int - top_y_int + 1, band.DataType)
                    # BandType = gdal.GetDataTypeName(band.DataType)
                    # ar = struct.unpack(fmttypes[BandType] * (bottom_y_int - top_y_int + 1) * (right_x_int - left_x_int + 1), scanarray)
                    #
                    # observations_pixels = np.array(ar)
                    # observations_pixels = np.reshape(observations_pixels, (bottom_y_int - top_y_int + 1, right_x_int - left_x_int + 1))

                    if observations_pixels is None or (np.count_nonzero(observations_pixels) == 0):
                        inform_progress('Pixel failed on band {0}!'.format(band.GetBand()))
                        # Something went wrong and read operation failed.
                        continue

                    sum = 0
                    total_weight = 0

                    for yp in range(len(observations_pixels)):
                        for xp in range(len(observations_pixels[0])):
                            # pixel coordinates in the whole image
                            x = xp + left_x_int
                            y = yp + top_y_int

                            c = x + 1 - left_x
                            e = right_x - x
                            if c > 1 or c < 0:
                                if e > 1 or e < 0:
                                    a = observations_gt[1]
                                else:
                                    a = observations_gt[1] * e
                            else:
                                a = observations_gt[1] * c

                            d = y + 1 - top_y
                            f = bottom_y - y
                            if d > 1 or d < 0:
                                if f > 1 or f < 0:
                                    b = abs(observations_gt[5])
                                else:
                                    b = abs(observations_gt[5]) * f
                            else:
                                b = observations_gt[1] * d

                            weight = a * b
                            total_weight += weight
                            sum += observations_pixels[yp][xp] * weight
                    observation_pixels = None

                    value = sum / total_weight
                    result_arrays['bands']['band_' + str(band.GetBand())]['pixels'][i][j] = value

    return error_msg, result_arrays


def pixel_inside_mask(pixel, masks, last_index=0):
    """
    Determine whether pixel is inside the given mask

    **Arguments**:

        pixel: Shapely Polygon

        masks: list of Shapely Polygons or Multipolygons

        last_index: the index at which the last pixel was found in masks; default 0

    **Returns**:

        error_msg: possible error messages; empty string if none happened

        inside: boolean; true if pixel is completely inside any of the masks

        index: of the item in masks that had pixel inside it
    """
    error_msg = ''
    inside = False

    if last_index:
        # Most likely pixel will be inside the same Multipolygon as the last one, so we check that first
        inside = True if masks[last_index].contains(pixel) else inside

        if not inside:  # the pixel wasn't inside mask
            # let's check other masks
            # start at 0, but skip last_index
            i = 0
            for m in masks:
                if i == last_index:
                    i += 1
                    continue
                inside = True if m.contains(pixel) else inside
                if inside:
                    last_index = i
                    break
                else:
                    i += 1
    else:  # no last_index was given (or it was 0), let's check all masks in order
        i = 0
        for m in masks:
            inside = True if pixel.within(m) else inside
            if inside:
                last_index = i
                break
            else:
                i += 1
    return error_msg, inside, last_index


def write_to_raster(raster_info, path):
    """
    Write the data in raster_info to raster. Raster will be written in byte form.

    **Arguments**:

        raster_info: dictionary with the following attributes: GeoTransform, Projection, Format, width, height, bands; where bands is dictionary with structure: { band_number: array_with_pixel_values }

        path: absolute target path for raster

    **Returns**:

        error_msg: possible error messages; empty string if none happened
    """
    error_msg = ''

    try:
        driver = gdal.GetDriverByName(raster_info['Format'])
        dataset = driver.Create(path, raster_info['width'], raster_info['height'], len(raster_info['bands']), gdal.GDT_Byte)
    except RuntimeError as e:  # some error happened while creating file
        error_msg += 'The following error happened while creating raster:\n' + str(e)
        return error_msg

    dataset.SetGeoTransform(raster_info['GeoTransform'])
    dataset.SetProjection(raster_info['Projection'])

    for band_number, dict in raster_info['bands'].items:
        dataset.GetRasterBand(int(band_number)).WriteArray(dict['pixels'])

    dataset = None  # save the created raster file
    return error_msg


def write_to_mat(data, path):
    """
    Writes data to mat-file using scipy

    **Arguments**:

        data: dictionary

        path: absolute path to file to be created

    **Returns**:

        error_msg: possible error messages; empty string if none happened
    """
    error_msg = ''

    try:
        sio.savemat(path, data)
    except RuntimeError as e:
        error_msg += 'The following error happened while creating mat-file:\n' + str(e)
    return error_msg


def calculate_glcm(data, angle, distance, levels, symmetric, inform_progress):
    """
    Calculates gray-level co-occurrance matrix for each band in data. Normalizes data in bands to be from 0 to levels.

    **Arguments**:

        data: data to be used in calculations. Must be dictionary with structure: { 'bands': { 'band_1': array, ... } }

        angle: angle for offset to be used in GLCM calculation as numpy float, eg. np.pi / 4

        distance: distance to be used in GLCM calculations as integer

        levels: maximum value to which data is normalized, max. 255

        symmetric: is the resulting GLCM symmetric, boolean

    **Returns**:

        error_msg: possible error messages; empty string if none happened

        data: original data appended with GLCMs
    """
    error_msg = ''

    # Give the used parameters with data so they can be used in further calculations
    data['glcm_levels'] = levels
    data['glcm_angle'] = angle
    data['glcm_distance'] = distance
    data['glcm_symmetric'] = symmetric

    for band, band_array in data['bands'].items():  # Calculate for each band
        # First normalize array data to be from 0...levels
        max_val = np.nanmax(band_array['pixels'])
        array = np.rint(band_array['pixels'] / max_val * levels)
        array = np.nan_to_num(array)
        result = greycomatrix(array, [distance], [angle], levels + 1, symmetric, True)  # the glcm is always normalized
        data['bands'][band]['glcm'] = result[:, :, 0, 0]

    return error_msg, data
